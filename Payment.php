<?php
namespace Travel;

Interface Payment 
{
	public function pay($company, string $place, float $fee);
}