<?php
namespace Travel;

class TravelAgency implements Payment 
{
	
	protected $guides = [];
	//travel itinerary, it is an object.
	protected $tour;
	//after pay money then they are agency's vistors.
	protected $tourists = [];
	protected $name;
	protected $address;
	protected $phone;

	function __construct(string $name, Tour $tour) 
	{
		$this->name = $name;
		$this->tour = $tour;
	}

	public function getName() 
	{
		return $this->name;
	}

	public function viewTour() 
	{
		$view = $this->tour->viewTour();
		return $view;
	}

	public function listTour() 
	{
		return $this->tour;
	}

	public function addGuide(Guide $guide) 
	{
		$this->guides[] = $guide;   
	}

	public function ListGuide() 
	{
		$say = '';
		foreach ($this->guides  as $key => $value) {
			$say .= $value->getName() . ":" . $value->getCertificate(). ":" .$value->listPlaces()."\n";
		}
		return $say;
	}


	public function addTourist(Tourist $tourist , string $place, float $fee = 0) 
	{
		$this->tourists[$place][] = $tourist;
	}

	public function listTourist() 
	{
		return $this->tourists;
	}

	public  function organizeGuide(Guide $guide, string $place , Hotel $hotel = null) 
	{
		$result = '';
		if (!$guide instanceof Guide ||  !in_array($guide, $this->guides)) {
			echo "this guide is not in the travel agency\n";
			return false;
		}
		if (strpos($guide->listPlaces(), $place) === false) {
			echo "this guide ". htmlentities($guide->getName()). " has no experience for the ".htmlentities($place)."\n";
			return false;
		}

		if (!array_key_exists($place, $this->tour->getTour())) {
			echo "the travel agency has no tour to this ".htmlentities($place)."\n";
			return false;  
		}

		$result =  $guide->guideTour($place, $this->tourists, $this->tour->getTour(), $hotel);
		return $result;
	}

	//implement interface function.
	public function pay($hotel, string $place, float $fee) 
	{
		$result = "";
		if (empty($place)) {
			echo "please add place as argument\n";
			return $result;
		}
		$result = "Travel agency paid $place city ".$hotel->getName()." hotel $fee\n";
		return $result;
	}

}