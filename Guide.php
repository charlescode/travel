<?php
namespace Travel;

class Guide extends Person 
{

	const DEGREE = ['Junior' , 'Senior', 'Professional'];
	protected $certificate;
	protected $rate;
	protected $name;
	protected $guidePlaces = [];

	function __construct(string $name, $certificate = 'Junior') 
	{
		$this->name = $name;
		$this->certificate = $certificate;
	} 


	public function getCertificate() 
	{
		return $this->certificate;
	}

	public function setCertificate(string $certificate) 
	{
		if (in_array($certificate, self::DEGREE)) {
			$this->certificate = $certificate;
			return $this;
		} else {
			throw new \Exception;
		}

	}

	public function setRate(string $rate) 
	{
		$this->rate = $rate;
		return $this;
	}

	public function getRate() 
	{
		return $this->rate;
	}

	public function indroduce(string $place) 
	{
		$introduce  = "$this->getName() introduce $place to tourists\n";
		return $introduce;
	}

	/**
	 * guide add a new place in his place list
	 */	
	public function addPlace(string $place) 
	{
		if (!in_array($place, $this->guidePlaces)) {
			$this->guidePlaces[] = $place;
		}
		return $this->guidePlaces;
	}

	public function listPlaces() : string
	{
		return implode(',', $this->guidePlaces);
	}


	/**
	 * $place is the travel place
	 * $tourists is the guild takes group of travellers
	 * $tours is the itinerary, it is an array, the key is the $place.
	 * if the itinerary has sleep key word, means the guild will assign room for tourists.
	 */ 
	public function guideTour(string $place, array $tourists = [], array $tours = [], Hotel $hotel = null) 
	{
		$result = "";
		if (!array_key_exists($place, $tourists)) {
			echo "no any tourists travel the ".htmlentities($place).", please check\n";
			return false;
		}

		if (!array_key_exists($place, $tours)) {
			echo "no any tour product to ".htmlentities($place)." in the travel agency, please check\n";
			return false;
		}
		$result ="Guide " .$this->getName()." takes visitor(s) travel:\n";
		foreach ($tours[$place] as $key => $value) {
			//normal travel 
			if (strpos($key, 'sleep') === false) {
				foreach ($tourists[$place] as $item => $val) {
					$result .= "visitor ". $val->getName() ." $value at datetime $key\n";
				}
			} 
			// sleep in hotel
			else {
				if ($hotel instanceof Hotel) {
					$result .=$value.$hotel->getName()." hotel\n";
					$tempArray = $tourists[$place];
					$result .= $hotel->settle($tempArray);
				} else {
					$result .= ''; 
				}
			}

		}

		return $result;
	}


}