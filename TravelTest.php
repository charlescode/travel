<?php 
use PHPUnit\Framework\TestCase;
use Travel\Tour;
use Travel\Tourist;
use Travel\TravelAgency;
use Travel\Guide;
use Travel\Hotel;
use Travel\Factory;

require('/var/www/html/practise/hospital/travel/Factory.php');
require('/var/www/html/practise/hospital/travel/Payment.php');
require('/var/www/html/practise/hospital/travel/Hotel.php');
require('/var/www/html/practise/hospital/travel/Person.php');
require('/var/www/html/practise/hospital/travel/Guide.php');
require('/var/www/html/practise/hospital/travel/Tour.php');
require('/var/www/html/practise/hospital/travel/TravelAgency.php');
require('/var/www/html/practise/hospital/travel/Tourist.php');


class TravelTest extends TestCase 
{
	protected $tour;

	public function setUp(){


	}

	public function testGuide() 
	{
		$guide = Factory::make('guide', 'Charles');
		$junior = $guide->getCertificate();
		$this->assertEquals('Junior', $junior);

		$guide->setCertificate('Senior');
		$this->assertEquals('Senior', $guide->getCertificate());

		$guide->addPlace('Beijing');
		$places = $guide->addPlace('Melbourne');
		$this->assertContains('Beijing' , $places);
		$this->assertEquals(['Beijing', 'Melbourne'] , $places);


	}

	public function testTour() 
	{
		$tour = Factory::make('tour', 'World Travel');
		$tourPlace = $tour->addTour('', ' ', 'new tour');
		$this->assertEmpty($tourPlace);
		$tour->addTour('Beijing', '9:00', 'tour Summer Palace, it is the best place in Beijing');
		$tour->addTour('Beijing', '12:00', 'tour The Forbbiden City, it is the largest royle place in the world');
		$tour->addTour('Beijing', '15:00', 'tour the Nest Bird, the 2008 Olymic Game Museum');
		$tour->addTour('Melbourne', '9:00', 'travel Yala River, it is the lake in city');
		$tour->addTour('Melbourne', '10:30', 'travel the Yulika Tower, the highest place can see every place in the city');
		$tourList = $tour->addTour('Melbourne', '15:00', 'travel the Penguin Island, it is to see the lovely Penguin');
		
		$this->assertArrayHasKey('Beijing', $tourList);
		echo htmlentities($tour->viewTour());
		echo htmlentities($tour->viewTour('Melbourne'));

		$tourList = $tour->addTour('Sydney', '8:30', 'The Opera House, the famous building in the world');
		// var_dump($tourList);
		$tour->deleteTour('Sydney');
		echo htmlentities($tour->viewTour());

		$this->tour = $tour;

	}

	public function testHotel() 
	{
		$availRooms = [
			'2ps' => 1,
			'2p' => 1,
			'3p' => 0,
			'4p' => 1,
			'4ps' => 1,
			];
		$hotel = Factory::make('hotel', 'Mantra Parramatta', 3);
		$this->assertEquals('Mantra Parramatta', $hotel->getName());
		$hotel->setAvailRooms($availRooms);
		$this->assertArrayHasKey('2ps', $hotel->getAvailRooms());

		$yingli = Factory::make('tourist' , 'Yingli');
		$yunhua = Factory::make('tourist', 'Yunhua');
		$huimin = Factory::make('tourist', 'Huimin');
		$this->assertInstanceOf(Tourist::class, $huimin);
		$tom = Factory::make('tourist', 'Tom');
		$chris = Factory::make('tourist', 'Chris');

		$tourists = [$yingli, $yunhua, $tom, $chris, $huimin];

		$return = $hotel->settle($tourists);
		// var_dump($return);

	}

	public function testTourist()
	{

		$tour = Factory::make('tour','World One Month Travel');
		$this->assertInstanceOf(Tour::class, $tour);
		$tour->addTour('Beijing', '9:00', 'tour(s) Summer Palace, it is the best place in Beijing');
		$tour->addTour('Beijing', '12:00', 'tour(s) The Forbbiden City, it is the largest royle place in the world');
		$tour->addTour('Beijing', '15:00', 'tour(s) the Nest Bird, the 2008 Olymic Game Museum');
		$tour->addTour('Beijing', '21:00 sleep', 'arrange visitor(s) sleep in ');
		
		$tour->addTour('Melbourne', '9:00', 'travel(s) Yala River, it is the lake in city');
		$tour->addTour('Melbourne', '10:30', 'travel(s) the Yulika Tower, the highest place can see every place in the city');
		$tourList = $tour->addTour('Melbourne', '15:00', 'travel(s) the Penguin Island, it is to see the lovely Penguin');
		$tourList = $tour->addTour('Melbourne', '20:00 sleep', 'arrange visitor(s) sleep in ');

		$agency = new TravelAgency('Sunny', $tour);
		

		$yingli = Factory::make('tourist', 'Yingli');
		$yingli->pay($agency, 'Melbourne', '2000');
		$visitor = Factory::make('tourist', 'Huimin');
		$visitor->pay($agency, 'Melbourne', '2100');
		
		$yunhua = Factory::make('tourist' , 'Yunhua');
		$yunhua->pay($agency, 'Beijing', '3000');

		$travelers = $agency->listTourist();
		// var_dump($travelers);
		$this->assertEquals('Yingli', $travelers['Melbourne'][0]->getName());
		$this->assertEquals('Yunhua', $travelers['Beijing'][0]->getName());
	
		$guide = Factory::make('guide', 'Xin');
		$guide->addPlace('Melbourne');

		$zhao = Factory::make('guide', 'Zhao');
		$zhao->setCertificate('Senior');
		$this->assertEquals('Senior', $zhao->getCertificate());
		$zhao->addPlace('Beijing');
		$zhao->addPlace('Hangzhou');
		$zhao->addPlace('Melbourne');
		
		
		$agency->addGuide($guide);
		$agency->addGuide($zhao);

		// var_dump($agency->listGuide());
		$this->assertContains('Xin' , $agency->listGuide());
		$this->assertContains('Hangzhou' , $agency->listGuide());
		// var_dump($tour);
		// var_dump($agency->listTourist());

		$availRooms = [
			'2ps' => 1,
			'2p' => 1,
			'3p' => 0,
			'4p' => 1,
			'4ps' => 1,
			];
		$hotel = Factory::make('hotel', 'Mantra Melbourne');
		$hotel->setStar('3');
		$this->assertEquals('3', $hotel->getStar());
		$hotel->setAvailRooms($availRooms);


		$result = $zhao->guideTour('Melbourne', $agency->listTourist(), $agency->listTour()->getTour(), $hotel);
		// var_dump($result);
		$result = $zhao->guideTour('Melbourne', $agency->listTourist(), $tour->getTour(), $hotel);
		// var_dump($result);

		$result = $agency->organizeGuide($zhao, 'Melbourne', $hotel);
		// var_dump($result);

		$result = $agency->organizeGuide($guide, 'Beijing', $hotel);
		$this->assertFalse($result);

		$result = $agency->organizeGuide($zhao, 'Beijing', $hotel);
		$this->assertContains('Summer Palace', $result);
		$this->assertContains('Nest Bird', $result);
		// var_dump($result);


		$guideWang = Factory::make('guide', 'Wang');
		$guideWang->addPlace('Melbourne');

		$result = $agency->organizeGuide($guideWang, 'Melbourne');
		$this->assertFalse($result);

		$result = $agency->pay($hotel, 'Melbourne', '5000');
		echo htmlentities($result);
		$this->assertContains('5000', $result);
	}

	public function tearDown() {


	}

}