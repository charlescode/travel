<?php
namespace Travel;

class Tour 
{
	protected $tour = [];
	protected $name;
	
	function __construct(string $name) 
	{
		$this->name = $name;
	}

	public function addTour(string $place, string $dateTime, string $description) : array 
	{
		if (empty($place) || empty($dateTime)) {
			return $this->tour;
		}
		if (!array_key_exists($place, $this->tour)) {
			$this->tour[$place] = [];
			$this->tour[$place][$dateTime] = $description;
 		} else {
 			if (!array_key_exists($dateTime, $this->tour[$place])) {
 				$this->tour[$place][$dateTime] = $description;
 			} 

 		}
 		return $this->tour;
	}

	public function viewTour($place = '')
	{
		$say = "";
		if ($place == '')	{
			foreach ($this->tour as $key => $value) {
				$say .= "[$key]";
				foreach ($value as $item  => $val) {
					$say .= "[date time: $item] [description: $val]\n";
				}

			}
		}

		else if (array_key_exists($place, $this->tour)) {
			foreach ($this->tour[$place] as $key => $value) {
				$say .="[$place][date time: $key] [description: $value]\n";
			}
			
		}
		return $say;
	}

	public function deleteTour($place) 
	{
		if (array_key_exists($place, $this->tour)) {
			unset($this->tour[$place]);
		}
		return $this->tour;
	}

	public function getTour() 
	{
		return $this->tour;
	}

}