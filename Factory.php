<?php

namespace Travel;

class Factory 
{
	
	public static function make(string $type, string $name) 
	{
		switch ($type) {
			case 'tourist':
				$object = new Tourist($name);
				break;
			case 'guide':
				$object  = new Guide($name);
				break;
			case 'hotel':
				$object = new Hotel($name);
				break;
			case 'tour':
				$object = new Tour($name);
				break;	

		}
		return $object;

	}

}



