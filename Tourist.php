<?php
namespace Travel;

class Tourist extends Person  implements Payment 
{
	//implement interface function.
	public function pay($travelAgency, string $place, float $fee = 0) 
	{
		if (empty($place)) {
			echo "no select travel distination place, please check\n";
			return;
		} 
		$travelAgency->addTourist($this, $place , $fee);

	}


}