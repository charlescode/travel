<?php
namespace Travel;

class Hotel 
{
	protected $name;
	protected $address;
	protected $star;
	protected $room = [
		'2ps' => '2 single beds',
		'2p' => '1 double bed',
		'3p' => '1 double bed, 1 single bed',
		'4p' => '2 double beds',
		'4ps' => '1 double bed, 2 single beds',
	];

	protected $roomPrice = [
		'2ps' => '200',
		'2p' => '200',
		'3p' => '280',
		'4p' => '500',
		'4ps' => '550',
	];

	protected $availRooms;

	function __construct(string $name, $star = '4', array $availRooms = []) 
	{
		$this->name = $name;
		$this->star = $star;
		$this->availRooms = $availRooms;
	}

	public function getStar() 
	{
		return $this->star;
	}

	public function setStar($star) 
	{
		$this->star = $star;
		return $this;
	}

	public function getName () 
	{
		return $this->name;
	}

	public function setAddress($place) 
	{
		$this->address = $place;
		return $this;
	}

	public function getAddress() 
	{
		return $this->address;
	}

	public function getAvailRooms() 
	{
		return $this->availRooms;
	}

	public function setAvailRooms ($rooms) 
	{
		$this->availRooms = $rooms;
		return $this;
	}

	//to calculate how many rooms the tourist will need, how many they should pay!
	public function settle($tourists = []) 
	{
		$assign = "";
		if (!count($tourists)) return "";
		foreach ($tourists as $key => $value) {
			if (! $value instanceof Tourist) {
				return "";
			}
		}
		foreach ($this->getAvailRooms() as $key => $value ) {
			$liveNumber = substr($key, 0, 1);
			//how many person can live in the room, such as 2 persons live the room.
			$number = $liveNumber * $value;
			for ($i = 0; $i < $number && count($tourists); $i++) {
				$tourist = array_shift($tourists);
				$assign .= "assgin ".$tourist->getName()." lives in ". $this->room[$key]." room and need to pay ". $this->roomPrice[$key]. "\n";
			}
		}
		if ($count = count($tourists)) {
			$assign .=" left ".$count. " visitors has no room sleep in ". $this->getName()."\n"; 
		}
	
		return $assign;
	}


}