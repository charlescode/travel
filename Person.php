<?php
namespace Travel;

class Person 
{
	protected $name;
	protected $age;
	protected $gender;

	function __construct(string $name) 
	{
		$this->name = $name;

	}

	public function getName() 
	{
		return $this->name;
	}

	
}